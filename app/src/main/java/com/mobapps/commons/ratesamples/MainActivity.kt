package com.mobapps.commons.ratesamples

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mobapps.commons.rate.helpers.askIfLiked
import com.mobapps.commons.rate.helpers.rateApp
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bt1.setOnClickListener { rateApp(thenFinish = true) }
        bt2.setOnClickListener { rateApp() }
        bt3.setOnClickListener { askIfLiked(thenFinish = true) }
        bt4.setOnClickListener { askIfLiked() }
    }
}
