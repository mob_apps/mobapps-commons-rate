## MobApps Rate

Para usar essa lib, você deve efetuar os seguintes passos .

## 1. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories:

```
 allprojects {
	 repositories {
		 ...
		 maven { url 'https://jitpack.io' }
	 }
 }
```


## 2. Add the dependency

```gradle
    	
 dependencies {
    implementation 'org.bitbucket.mob_apps:mobapps-commons-rate:0.0.03'
 }
```
[![](https://jitpack.io/v/org.bitbucket.mob_apps/mobapps-commons-rate.svg)](https://jitpack.io/#org.bitbucket.mob_apps/mobapps-commons-rate)


## 3. Exemplo de uso estão nesse link

https://bitbucket.org/mob_apps/mobapps-commons-rate/src/master/app/src/main/java/com/mobapps/commons/ratesamples/

