package com.mobapps.commons.rate.helpers

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.mobapps.commons.mobapps_ktx.exts.appName
import com.mobapps.commons.mobapps_ktx.exts.dpToPx
import com.mobapps.commons.mobapps_ktx.exts.launchGooglePlay
import com.mobapps.commons.rate.R
import com.mobapps.commons.rate.ui.RateAlertDialog

private val preferenceKey = "rate_preferences"
private val hasShowedRateAppKey = "has_showed_ask_if_liked"
private val hasShowedAskIfLikedKey = "has_showed_ask_if_liked"

fun AppCompatActivity.rateApp(thenFinish: Boolean = false){
    RateAlertDialog()
        .setImageResource(R.drawable.img_play_star)
        .setTitle(R.string.rate_app_title)
        .setDescription(R.string.rate_app_description)
        .setNeutralButton(R.string.rate_app_neutral_title, object : RateAlertDialog.OnNeutralButtonClickListener {
            override fun onNeutralButtonClick(rateAlertDialog: RateAlertDialog) {
                rateAlertDialog.dismiss()
            }
        })
        .setPositiveButton(R.string.rate_app_positive_title, object : RateAlertDialog.OnPositiveButtonClickListener {
            override fun onPositiveButtonClick(rateAlertDialog: RateAlertDialog) {
                launchGooglePlay()
                rateAlertDialog.dismiss()
            }
        })
        .setDismissListener(object : RateAlertDialog.OnDismissListener {
            override fun onDismiss() {
                if (thenFinish)
                    finish()
            }
        })
        .show(supportFragmentManager, "")
}

fun AppCompatActivity.askIfLiked(thenFinish: Boolean = false) {
    val builder = MaterialAlertDialogBuilder(this)
            .setTitle("Você gostou do ${appName()}?")
            .setPositiveButton("Sim") { _, _ ->
                rateApp(thenFinish)
            }
            .setNegativeButton("Não") { _, _ ->
                if (thenFinish){
                    finish()
                    return@setNegativeButton
                }
                val snackbar = Snackbar.make(window.decorView.rootView, "Obrigado pelo feedback", Snackbar.LENGTH_LONG)
                val snackBarView = snackbar.view;
                snackBarView.translationY = (-48).dpToPx()
                snackbar.show()
            }

    val dialog = builder.create()
    if (!this.isFinishing) {
        dialog.show()
    }
}

fun AppCompatActivity.rateAppOnce(thenFinish: Boolean = false, key: String): Boolean {
    val preferences = getSharedPreferences(preferenceKey, Context.MODE_PRIVATE)
    val hasShowed = preferences.getBoolean(key, false)
    var willShow = false
    if (!hasShowed) {
        willShow = true
        rateApp(thenFinish)
        preferences.edit().putBoolean(key, true).apply()
    }
    return willShow
}

fun AppCompatActivity.askIfLikedOnce(thenFinish: Boolean = false, key: String): Boolean {
    val preferences = getSharedPreferences(preferenceKey, Context.MODE_PRIVATE)
    val hasShowed = preferences.getBoolean(key, false)
    var willShow = false
    if (!hasShowed) {
        willShow = true
        askIfLiked(thenFinish)
        preferences.edit().putBoolean(key, true).apply()
    }
    return willShow
}
