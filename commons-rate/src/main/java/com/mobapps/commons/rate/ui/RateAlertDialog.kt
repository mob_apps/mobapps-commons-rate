package com.mobapps.commons.rate.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.button.MaterialButton
import com.mobapps.commons.mobapps_ktx.exts.hideView
import com.mobapps.commons.mobapps_ktx.exts.setVisible
import com.mobapps.commons.rate.R

class RateAlertDialog: DialogFragment() {

    @StringRes private var titleRes: Int? = null
    @StringRes private var descriptionRes: Int? = null

    @DrawableRes private var imageResource: Int? = null
    @StringRes private var positiveButtonTitleRes: Int? = null
    @StringRes private var negativeButtonTitleRes: Int? = null
    @StringRes private var neutralButtonTitleRes: Int? = null

    private var title: String? = null
    private var description: String? = null

    private var positiveButtonTitle: String? = null
    private var negativeButtonTitle: String? = null
    private var neutralButtonTitle: String? = null


    private var hasPositiveButton: Boolean = false
    private var hasNegativeButton: Boolean = false
    private var hasNeutralButton: Boolean = false

    private var onPositiveButtonClickListener: OnPositiveButtonClickListener? = null
    private var onNegativeButtonClickListener: OnNegativeButtonClickListener? = null
    private var onNeutralButtonClickListener: OnNeutralButtonClickListener? = null
    private var onDismissListener: OnDismissListener? = null
    private var themeRes: Int? = null

    private var tvTitle: AppCompatTextView? = null
    private var tvDescription: AppCompatTextView? = null
    private var ivImage: AppCompatImageView? = null
    private var btPositive: MaterialButton? = null
    private var btNegative: MaterialButton? = null
    private var btNeutral: MaterialButton? = null


    override fun getTheme(): Int {
        if (themeRes!=null)
            return themeRes as Int

        return super.getTheme()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_fragment_rate, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvTitle = view.findViewById(R.id.tvTitle)
        tvDescription = view.findViewById(R.id.tvDescription)
        ivImage = view.findViewById(R.id.ivImage)
        btPositive = view.findViewById(R.id.btnActionPositive)
        btNegative = view.findViewById(R.id.btnActionNegative)
        btNeutral = view.findViewById(R.id.btnActionNeutral)
        handleViews()
    }

    fun setTheme(themeRes: Int): RateAlertDialog {
        this.themeRes = themeRes
        return this
    }

    fun setTitle(@StringRes titleRes: Int): RateAlertDialog {
        this.titleRes = titleRes
        return this
    }

    fun setDescription(@StringRes descriptionRes: Int): RateAlertDialog {
        this.descriptionRes = descriptionRes
        return this
    }

    fun setTitle(title: String): RateAlertDialog {
        this.title = title
        return this
    }

    fun setDescription(description: String): RateAlertDialog {
        this.description = description
        return this
    }

    fun setImageResource(@DrawableRes imageResource: Int) : RateAlertDialog {
        this.imageResource = imageResource
        return this
    }

    fun setPositiveButton(@StringRes textId: Int, onPositiveButtonClickListener: OnPositiveButtonClickListener? = null) : RateAlertDialog {
        this.hasPositiveButton = true
        this.onPositiveButtonClickListener = onPositiveButtonClickListener
        this.positiveButtonTitleRes = textId
        return this
    }

    fun setNegativeButton(@StringRes textId: Int, onNegativeButtonClickListener: OnNegativeButtonClickListener?) : RateAlertDialog {
        this.hasNegativeButton = true
        this.onNegativeButtonClickListener = onNegativeButtonClickListener
        this.negativeButtonTitleRes = textId
        return this
    }

    fun setNeutralButton(@StringRes textId: Int, onNeutralButtonClickListener: OnNeutralButtonClickListener?) : RateAlertDialog {
        this.hasNeutralButton = true
        this.onNeutralButtonClickListener = onNeutralButtonClickListener
        this.neutralButtonTitleRes = textId
        return this
    }

    fun setPositiveButton(text: String, onPositiveButtonClickListener: OnPositiveButtonClickListener? = null) : RateAlertDialog {
        this.hasPositiveButton = true
        this.onPositiveButtonClickListener = onPositiveButtonClickListener
        this.positiveButtonTitle = text
        return this
    }

    fun setNegativeButton(text: String, onNegativeButtonClickListener: OnNegativeButtonClickListener?) : RateAlertDialog {
        this.hasNegativeButton = true
        this.onNegativeButtonClickListener = onNegativeButtonClickListener
        this.negativeButtonTitle = text
        return this
    }

    fun setNeutralButton(text: String, onNeutralButtonClickListener: OnNeutralButtonClickListener?) : RateAlertDialog {
        this.hasNeutralButton = true
        this.onNeutralButtonClickListener = onNeutralButtonClickListener
        this.neutralButtonTitle = text
        return this
    }

    fun setDismissListener(onDismissListener: OnDismissListener): RateAlertDialog {
        this.onDismissListener = onDismissListener
        return this
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            super.show(manager, tag)
        } catch (e: Exception){
            e.printStackTrace()
        }
    }

    private fun handleViews() {
        handleTitle()
        handleDescription()
        handleImage()
        handleButtons()
    }

    private fun handleButtons() {

        btPositive?.setVisible(hasPositiveButton)
        btNegative?.setVisible(hasNegativeButton)
        btNeutral?.setVisible(hasNeutralButton)

        positiveButtonTitleRes?.let { btPositive?.setText(it) }
        negativeButtonTitleRes?.let { btNegative?.setText(it) }
        neutralButtonTitleRes?.let { btNeutral?.setText(it) }

        positiveButtonTitle?.let { btPositive?.setText(it) }
        negativeButtonTitle?.let { btNegative?.setText(it) }
        neutralButtonTitle?.let { btNeutral?.setText(it) }

        btPositive?.setOnClickListener {
            onPositiveButtonClickListener?.onPositiveButtonClick(this)
        }

        btNegative?.setOnClickListener {
            onNegativeButtonClickListener?.onNegativeButtonClick(this)
        }

        btNeutral?.setOnClickListener {
            onNeutralButtonClickListener?.onNeutralButtonClick(this)
        }
    }

    private fun handleTitle() {
        if (titleRes == null && title == null){
            tvTitle?.hideView()
            return
        }

        titleRes?.run { tvTitle?.setText(this) }
        title?.run { tvTitle?.setText(this) }
    }

    private fun handleDescription() {
        if (descriptionRes == null && description == null) {
            tvDescription?.hideView()
            return
        }

        descriptionRes?.run { tvDescription?.setText(this) }
        description?.run { tvDescription?.setText(this) }
    }

    private fun handleImage() {
        if (imageResource == null){
            ivImage?.hideView()
            return
        }

        ivImage?.setImageResource(imageResource!!)
    }

    override fun onDismiss(dialog: DialogInterface) {
        onDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    interface OnPositiveButtonClickListener{
        fun onPositiveButtonClick(rateAlertDialog: RateAlertDialog)
    }

    interface OnNegativeButtonClickListener {
        fun onNegativeButtonClick(rateAlertDialog: RateAlertDialog)
    }

    interface OnNeutralButtonClickListener {
        fun onNeutralButtonClick(rateAlertDialog: RateAlertDialog)
    }

    interface OnDismissListener {
        fun onDismiss()
    }

}